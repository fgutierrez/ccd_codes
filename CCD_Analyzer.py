#Let's import first the main libraries
import params_analyzer
import os
import glob
import numpy as np
#from numpy import unravel_index
from astropy.io import fits
#import matplotlib.pyplot as plt
#import matplotlib.pyplot as plt

#Let's create the class that analyses the data
class CCD_processor:
    """
    This class analyses the data that has been irradiated with a given radiation scheme
    
    """
    def __init__(self, data_dir, output_dir, gain, integration_times=None):
        """_Creates the first method_

        Args:
            data_dir (_str_): _Path to the directory with the data_
            output_dir (_str_): _Path to the directory to store the outputs in_
            integration_times (_numpy array_): _integration time values_
            gain (int, optional): _Value of the gain_. Defaults to 1.
        """
        self.data_dir = data_dir
        self.output_dir = output_dir
        self.integration_times = integration_times
        self.gain = gain
        self.fits_files = {}
        self.left_avg = None
        self.right_avg = None
        self.cti_regions = {}
    
    def offset_obtainer(self, data):
        # Select the regions where to obtain the offset from
        left_offset_region = data[params_analyzer.left_offset_row_in:params_analyzer.left_offset_row_end, params_analyzer.left_offset_col_in:params_analyzer.left_offset_col_end]
        right_offset_region = data[params_analyzer.right_offset_row_in:params_analyzer.right_offset_row_end, params_analyzer.right_offset_col_in:params_analyzer.right_offset_col_end]
        # Calculate electronic offset for each half form the regions selected above
        e_offset_left = np.nanmean(left_offset_region)
        e_offset_right = np.nanmean(right_offset_region)
        return e_offset_left, e_offset_right
        
        
    def _ccd_preprocessing(self, data, offset_left_half, offset_right_half):
        # We obtain the offset from each half of the CCD
        offset_left_half, offset_right_half = self.offset_obtainer(data)
        #We obtain the overscan rows for each half of the CCD of each firs file
        left_half_overscanrows = data[params_analyzer.left_row_in:, :params_analyzer.left_col_end]
        right_half_overscanrows = data[params_analyzer.right_row_in:, params_analyzer.right_col_in:]
        # We remove the offset and then multiply by the gain on every overscan region
        pre_processed_left = (left_half_overscanrows- offset_left_half) * self.gain
        pre_processed_right = (right_half_overscanrows - offset_right_half) * self.gain
        return pre_processed_left, pre_processed_right
    
    def get_averaged_halves(self):
        """_This is a function that takes all the fits files (exposures) of a given integration time and pre process each one of them
        half wise_

        Returns:
            _right_avg_list (list)_: _A list with 51 numpy arrays (averaged), one for each integration time and corresponging CCD half._
            _left_avg_list (list)_: _A list with 51 numpy arrays (averaged), one for each integration time and corresponding CCD half._
        """
        
        left_avg_list = []
        right_avg_list = []

        if '633nm' in self.data_dir:  # For the first dataset
            for int_time in self.integration_times:
                fits_list = glob.glob(os.path.join(self.data_dir, f'*int{int_time:.2f}*.fits'))
                left_half_list = []
                right_half_list = []

                for fits_file in fits_list:
                    hdu = fits.open(fits_file)
                    data = hdu[0].data
                    offset_left, offset_right = self.offset_obtainer(data)
                    preprocessed_left, preprocessed_right = self._ccd_preprocessing(data, offset_left, offset_right)
                    left_half_list.append(preprocessed_left)
                    right_half_list.append(preprocessed_right)

                avg_left_half = np.mean(left_half_list, axis=0)
                avg_right_half = np.mean(right_half_list, axis=0)                     
                
                left_avg_list.append(avg_left_half)
                right_avg_list.append(avg_right_half)
                
        else:  # For the second dataset (Prodhomme dataset)               
            for file_number in range(41, 78):
                fits_file = os.path.join(self.data_dir, f'eper-3_{file_number:04d}.fits')
                hdu = fits.open(fits_file)
                data = hdu[0].data
                offset_left, offset_right = self.offset_obtainer(data)
                preprocessed_left, preprocessed_right = self._ccd_preprocessing(data, offset_left, offset_right)           
                left_avg_list.append(preprocessed_left)
                right_avg_list.append(preprocessed_right)
                
        return left_avg_list, right_avg_list
            
            
# Usage example
#integration_times = np.arange(0.0, 5.1, 0.1)  # Integration times from 0 to 5 seconds
analyzer = CCD_processor(data_dir=params_analyzer.data_dir, output_dir=params_analyzer.output_dir, gain=params_analyzer.gain)
left_avg_list, right_avg_list = analyzer.get_averaged_halves()

# Save the entire lists as .npy files
np.save(params_analyzer.output_dir + 'left_avg_list.npy', left_avg_list)
np.save(params_analyzer.output_dir + 'right_avg_list.npy', right_avg_list)