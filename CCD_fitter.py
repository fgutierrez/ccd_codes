import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import params_fit

#Let's make a class with different versions of the cti functions
class CTIFunction:
    def __init__(self, num_terms):
        self.num_terms = num_terms
        self.params = None
        
    def cti(self, x, *args):
        a = args[:self.num_terms]
        b = args[self.num_terms:2*self.num_terms]
        c = args[2*self.num_terms]
        f = np.sum([a[i] * np.exp(-b[i] * x) for i in range(self.num_terms)], axis=0) + c
        return f
        
    def fit(self, x, y, bounds, sigma, guess=None):
        self.params, cov = curve_fit(self.cti, x, y, bounds=bounds, maxfev=99999999, sigma=sigma, p0=guess)
        return self.params, cov
    
    def reduced_chi_square(self, x, y, sigma, num_terms):
        fit = self.cti(x, *self.params)
        chi_square = np.sum(((y - fit)/ sigma) ** 2)
        degrees_freedom = len(y) - num_terms
        red_chi_square = chi_square / degrees_freedom
        return red_chi_square

    def plot_fit(self, x, y, sigma, s):
        if self.params is None:
            raise ValueError("Fit parameters are not available. Fit the data first.")

        fit = self.cti(x, *self.params)
        residuals = (y - fit) / sigma

        fig, axs = plt.subplots(2, 1, sharex=True, figsize=(6, 8), gridspec_kw={'height_ratios': [3, 1]})
        fig.suptitle(s, fontsize=16)  # Add a common title

        axs[0].plot(x, y, 'o', label='Data')
        axs[0].plot(x, fit, '-', label='Fit')
        axs[0].tick_params(axis='x', which='both', length=0)
        axs[0].legend(loc='best')
        axs[0].set_ylabel('Signal level [e-]')

        axs[1].plot(x, residuals, 'ro')
        axs[1].axhline(y=0, color='black', linestyle='-')
        axs[1].set_xticks([0, 250, 500])
        axs[1].set_xlabel('Row index')
        axs[1].set_ylabel('Weighted Residual')

        plt.subplots_adjust(wspace=0.7, hspace=0.05)
        plt.show()

#Usage example
x = params_fit.x
cti_IV = np.load(params_fit.data_dir + 'cti_IV.npy')
sigma_IV = np.load(params_fit.data_dir + 'sigma_IV.npy')
guess_IV = params_fit.guess_iv

# Create an instance of CTIFunction
cti = CTIFunction(num_terms=4)
# Fit the data using the specified guess parameter
paras_IV, cov_IV = cti.fit(x, cti_IV, bounds=(0, np.inf), sigma=sigma_IV, guess=guess_IV)

# Compute the reduced chi square
red_chi_2 = cti.reduced_chi_square(x, cti_IV, sigma_IV, 4)
print(red_chi_2)

# Plot the fit alongside the data with residuals
cti.plot_fit(x=x, y=cti_IV, sigma=sigma_IV, s='CCD region IV (4 trap species)')