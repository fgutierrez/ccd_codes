# Let's create the directory where to load the lists and where to save the regions
data_dir = '/home/fercho/Documents/Gitlab/ccd_codes/npy_files_prodhomme/'

# Let's define the numbers used for slicing the regions (number of columns to average)
I_in = 0
I_end = 900

II_in = 1100
II_end = 1800

IV_in = 750
IV_end = 1450

V_in = 1600
V_end = 2400

#Uncertainty parameters
readout_noise = 35
n_exp = 1