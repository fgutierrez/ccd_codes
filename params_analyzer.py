# Let's create the directory where to load the CCD data
data_dir = '/home/fercho/Documents/PhD/CCD/PLATO_CCD_CTI/CCD270_11363-05-01_20150830/flatfield/'
# Let's create the directory where to save the npy files with the lists
output_dir = '/home/fercho/Documents/Gitlab/ccd_codes/npy_files_prodhomme/'
# Let's create the directory where to put the plots of the raw data
#raw_dir = '/home/fercho/Documents/Gitlab/ccd_codes/plots_raw_data/prodhomme'
# Let's create the directory where to put the plots of the corrected data
#corr_dir = '/home/fercho/Documents/Gitlab/ccd_codes/plots_corrected_data/prodhomme'

# Let's declare the numbers for the overscan region
left_row_in = 4510
left_col_end = 2500
right_col_in = 2501
right_row_in = 4510

# Let's declare the numbers for the offset region
left_offset_col_in = 2280
left_offset_col_end = 2499
right_offset_col_in = 2500
right_offset_col_end = 2719

left_offset_row_in = 4510
left_offset_row_end = 4700
right_offset_row_in = 4510
right_offset_row_end = 4700
#Let's declare some CCD quantities
gain = 25


