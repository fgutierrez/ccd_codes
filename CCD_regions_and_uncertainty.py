import numpy as np
import params_regions

class RadiationRegionAnalyzer:
    def __init__(self, left_overscan_rows, right_overscan_rows, readout_noise, n_exp):
        self.left_overscan_rows = left_overscan_rows
        self.right_overscan_rows = right_overscan_rows
        self.readout_noise = readout_noise
        self.n_exp = n_exp
        self.cti_I = None
        self.cti_II = None
        self.cti_IV = None
        self.cti_V = None
        self.sigma_I = None
        self.sigma_II = None
        self.sigma_IV = None
        self.sigma_V = None

    def calculate_regions(self):
        self.cti_I = np.mean(self.left_overscan_rows[:, params_regions.I_in:params_regions.I_end], axis=1)
        self.cti_II = np.mean(self.left_overscan_rows[:, params_regions.II_in:params_regions.II_end], axis=1)
        self.cti_IV = np.mean(self.right_overscan_rows[:, params_regions.IV_in:params_regions.IV_end], axis=1)
        self.cti_V = np.mean(self.right_overscan_rows[:, params_regions.V_in:params_regions.V_end], axis=1)
        
    def uncertainty(self, data, n_col, n_exp):
        sigma = 1 / np.sqrt(n_col * n_exp) * np.sqrt(data + self.readout_noise ** 2)
        return sigma

    def calculate_sigmas(self):
        if self.cti_I is not None:
            self.sigma_I = self.uncertainty(self.cti_I, self.left_overscan_rows.shape[1], self.n_exp)
        if self.cti_II is not None:
            self.sigma_II = self.uncertainty(self.cti_II, self.left_overscan_rows.shape[1], self.n_exp)
        if self.cti_IV is not None:
            self.sigma_IV = self.uncertainty(self.cti_IV, self.right_overscan_rows.shape[1], self.n_exp)
        if self.cti_V is not None:
            self.sigma_V = self.uncertainty(self.cti_V, self.right_overscan_rows.shape[1], self.n_exp)
    
    def save_arrays(self):
        if self.cti_I is not None:
            np.save(params_regions.data_dir + 'cti_I.npy', self.cti_I)
        if self.sigma_I is not None:
            np.save(params_regions.data_dir + 'sigma_I.npy', self.sigma_I)
        if self.cti_II is not None:
            np.save(params_regions.data_dir + 'cti_II.npy', self.cti_II)
        if self.sigma_II is not None:
            np.save(params_regions.data_dir + 'sigma_II.npy', self.sigma_II)
        if self.cti_IV is not None:
            np.save(params_regions.data_dir + 'cti_IV.npy', self.cti_IV)
        if self.sigma_IV is not None:
            np.save(params_regions.data_dir + 'sigma_IV.npy', self.sigma_IV)
        if self.cti_V is not None:
            np.save(params_regions.data_dir + 'cti_V.npy', self.cti_V)
        if self.sigma_V is not None:
            np.save(params_regions.data_dir + 'sigma_V.npy', self.sigma_V)


# Example usage
#left_overscan_rows = np.load(params_regions.data_dir + 'left_avg_list.npy')[12]  # Example data for left overscan rows
#right_overscan_rows = np.load(params_regions.data_dir + 'right_avg_list.npy')[12] # Example data for right overscan rows
left_overscan_rows = np.mean(np.load(params_regions.data_dir + 'left_avg_list.npy'), axis=0)  # Example data for left overscan rows
right_overscan_rows = np.mean(np.load(params_regions.data_dir + 'right_avg_list.npy'), axis=0) # Example data for right overscan rows
readout_noise = params_regions.readout_noise
n_exp = params_regions.n_exp

analyzer = RadiationRegionAnalyzer(left_overscan_rows, right_overscan_rows, readout_noise, n_exp)
analyzer.calculate_regions()
analyzer.calculate_sigmas()
analyzer.save_arrays()


